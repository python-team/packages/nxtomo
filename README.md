# nxtomo

the goal of this project is to provide a powerful and user friendly API to create and edit [NXtomo](https://manual.nexusformat.org/classes/applications/NXtomo.html) application


Please find at https://tomotools.gitlab-pages.esrf.fr/nxtomo the latest documentation

Tutorials are avaible here: https://tomotools.gitlab-pages.esrf.fr/nxtomo/
