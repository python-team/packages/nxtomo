"""nexus path used to define a `NXmonitor <https://manual.nexusformat.org/classes/base_classes/NXmonitor.html>`_"""


class NEXUS_MONITOR_PATH:
    DATA_PATH = "data"


class NEXUS_MONITOR_PATH_V_1_0(NEXUS_MONITOR_PATH):
    pass


class NEXUS_MONITOR_PATH_V_1_1(NEXUS_MONITOR_PATH_V_1_0):
    pass


class NEXUS_MONITOR_PATH_V_1_2(NEXUS_MONITOR_PATH_V_1_1):
    pass


class NEXUS_MONITOR_PATH_V_1_3(NEXUS_MONITOR_PATH_V_1_2):
    pass


class NEXUS_MONITOR_PATH_V_1_4(NEXUS_MONITOR_PATH_V_1_3):
    pass
